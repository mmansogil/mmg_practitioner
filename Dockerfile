# Imagen raiz

FROM node

# Carpeta Raiz

WORKDIR /apitechu

#Copia de archivos de carpeta local a apitechu.

ADD . /apitechu

#instalación de dependencias

RUN npm install

#Puerto que espone apitechu

EXPOSE 3000

#Comando que se ejecuta con el RUN en la inicialización del Docker

CMD ["npm","start"]
