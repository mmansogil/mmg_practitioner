require('dotenv').config();
const express = require('express');// contiene el framework
const app = express();// iniciamos el framework
app.use(express.json());

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
var enableCORS = function(req,res,next){
  res.set("Access-Control-Allow-Origin","*");
  res.set("Access-Control-Allow-Methods","POST,GET,OPTIONS,DELETE,PUT");
  res.set("Access-Control-Allow-Headers","Content-Type");
  next();
}
app.use(express.json());
app.use(enableCORS);

const port = process.env.PORT || 3000; // mete en la variable  puerto de variable de entorno si existe sino 3000
app.listen(port);// arranca el servidor
console.log("API escuchando en el puerto lala tralala nose que" + port);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUsersByIdV2);
app.get('/apitechu/v1/account/:id',accountController.getAccountById);
app.post('/apitechu/v1/users',userController.createUsersV1);
app.post('/apitechu/v2/users',userController.createUsersV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);
app.post('/apitechu/v1/login/',authController.loginV1);
app.post('/apitechu/v2/login/',authController.loginV2);
app.post('/apitechu/v1/logout/:id',authController.logoutV1);
app.post('/apitechu/v2/logout/:id',authController.logoutV2);
app.get('/apitechu/v1/hello',
function(req, res){
 console.log("GET /apitechu/v1/hello");

 res.send({"msg" : "Hola caracola desde API TechU"});
}
)
