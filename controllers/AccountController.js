const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountById(req,res){
  console.log("GET /apitechu/v1/account/:id");
    var httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente Creado");
      var id = req.params.id;
        console.log("id de cuenta a traer" + id);
        var query = 'q={"userid":'+id+'}';
        httpClient.get ("account?" + query + "&" + mLabAPIKey,
        function (err,resMLab,body)
        {
          if (err){
            var response = {
              "msg": "Error obtenien docuenta de usuario"
            }
          res.status(500);
          }else{
           if(body.length > 0){
              var response= body;
           }else{
              var response = {
                "msg" : "error cuenta"
            }
            res.status(404);
            }
          }
        res.send(response);
        }
      )
}


module.exports.getAccountById = getAccountById;
