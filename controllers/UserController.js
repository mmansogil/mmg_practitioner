const io = require('../io');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente Creado");
  httpClient.get ("user?" + mLabAPIKey,
    function (err,resMLab,body)
    {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuario"
      }
      res.send(response);
    }
  )
}

function getUsersByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente Creado");
  var id = req.params.id;
  console.log("id de usuario a traer" + id);
  var query = 'q={"id":'+id+'}';
  httpClient.get ("user?" + query + "&" + mLabAPIKey,
    function (err,resMLab,body)
    {
      if (err){
        var response = {
          "msg": "Error obteniendo usuario"
        }
      res.status(500);
      }else{
       if(body.length > 0){
          var response= body[0];
       }else{
          var response = {
            "msg" : "Usuario no encontrado"
        }
        res.status(404);
        }
      }
    res.send(response);
    }
  )
}

function createUsersV1(req, res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email":req.body.email
  };
  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);
  console.log("usario añadido");
}

function createUsersV2(req, res){
  console.log("POST /apitechu/v2/users");
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);
  var newUser = {
    "id":req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email":req.body.email,
    "password":crypt.hash(req.body.password)
  };
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente Creado");

  httpClient.post("user?" + mLabAPIKey,newUser,
    function (err,resMLab,body)
    {
      var response = !err ? body : {
        "msg" : "Error creando usuario"
      }
      res.status(201).send("usuario creado");
    }
  )
}





function deleteUserV1(req, res) {
 console.log("DELETE /apitechu/v1/users/:id");
 console.log("id es " + req.params.id);

 var users = require('../usuarios.json');
 var deleted = false;
console.log("Usando Array findIndex");
var indexOfElement = users.findIndex(
  function(element){
    console.log("comparando " + element.id + " y " +   req.params.id);
    return element.id == req.params.id
  }
)

 console.log("indexOfElement es " + indexOfElement);
 if (indexOfElement >= 0) {
   users.splice(indexOfElement, 1);
   deleted = true;
 }

if (deleted) {
  // io.writeUserDataToFile(users);
  io.writeUserDataToFile(users);
}

var msg = deleted ?
"Usuario borrado" : "Usuario no encontrado."

console.log(msg);
res.send({"msg" : msg});
}



module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
module.exports.deleteUserV1 = deleteUserV1;
