const io = require('../io');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt')

function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}





function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");
 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Cliente Creado");
  console.log(req.body.email);
 var email = req.body.email;
   var query = 'q={"email":"'+ email +'"}';
   var putBody = '{"$set":{"logged":true}}';

   httpClient.get ("user?" + query + "&" + mLabAPIKey,
     function (err,resMLab,body)
     {
       if (err){
         var resp = {
           "msg": "Error obteniendo usuario"
         }
       res.status(500);
       }else{
        if(body.length > 0){
           var response= body[0];

            if(crypt.checkPassword(req.body.password,response.password)){
                 httpClient.put ("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
                  function (MLerr,MLresMLab,MLbody)
                    {
                          console.log(response.password + "correcto");
                      if (!MLerr)
                          {
                            var resp = {
                              "msg" : "Usuario logado con éxito",
                              "idUsuario" : body[0].id
                            }
                          res.send(resp);
                        }else{
                          var resp =  {
                            "msg" : "Error base de datos"
                        }
                        res.status(500);
                      }
                    }
                  )
             }else{
               resp =  {
                  "msg" : "Password Incorrecta"
               }
               res.send(resp);
               res.status(401);
             }
        }else{
           resp = {
             "msg" : "Usuario no encontrado"
         }
          res.send(resp);
         res.status(404);
         }
       }

     }
 )

 }









function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout/:id");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}



function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Cliente Creado");
 var id = req.params.id;
 console.log("id de usuario a traer" + id);
 var query = 'q={"id":'+id+',"logged":true }';
  var putBody = '{"$unset":{"logged":""}}';
 httpClient.get ("user?" + query + "&" + mLabAPIKey,
   function (err,resMLab,body)
   {
     if (err){
       var response = {
         "msg": "Error obteniendo usuario"
       }

        res.send(response);
     }else{
      if(body.length > 0){
        httpClient.put ("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
         function (MLerr,MLresMLab,MLbody){
           var response = {
             "msg" : "Usuario logout con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
      }else{
         var response = {
           "msg" : "Usuario no encontrado"
       }
       res.status(404);

       }
     }

   }
 )
}







module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
